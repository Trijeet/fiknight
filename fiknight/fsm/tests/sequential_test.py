##Imports
from fiknight.fsm.core import *
from fiknight.fsm.common.events import *
from fiknight.fsm.common.generator import *

##Build a three cycle machine.
fsm = sequentialMachine("Prototype", n_states=3)

##Walk through the machine.
for i in range(3):
    fsm.current_state.resolveEventTransition("baseTransition")
    print(fsm.current_state.name)
    print(fsm.history)
