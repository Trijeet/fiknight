#!/usr/bin/env python
# -*- coding: utf-8 -*-

##Package imports:
from fiknight.fsm.core import *
from fiknight.fsm.common.events import *
from fiknight.fsm.export.networkx import *

def main():
    '''Entry point for FSM program.'''
    ##Build a three cycle machine.
    # fsm = Machine.sequentialMachine("Prototype", n_states=3)
    # x = Event(name="bepsi", destination_weights=(5), priority="flo")
    fsm = Machine.fullyConnected("Prototype", n_states=3)

    ##Walk through the machine.
    for i in range(3):
        fsm.current_state.resolveEventTransition("baseTransition")
        print(fsm.current_state.name)
        print(fsm.history)


if __name__ == '__main__':
	main()