import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name="fiknight",
    version="0.0.1",
    author="Trijeet Sethi",
    author_email="trijeets@gmail.com",
    description="A simple package implementing a finite state machine.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Trijeet/fiknight",
    packages=setuptools.find_packages(),
     entry_points={
        'console_scripts': [
            'my_project = my_project.__main__:main'
        ]
      },
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
        "Development Status :: 2 - Pre-Alpha",
        "Natural Language :: English",
    ],
    python_requires='>=3.6',
)